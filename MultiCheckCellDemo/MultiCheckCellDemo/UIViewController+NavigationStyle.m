//
//  UIViewController+NavigationStyle.m
//  kdweibo
//
//  Created by sevli on 16/9/8.
//  Copyright © 2016年 www.kingdee.com. All rights reserved.
//

#import <objc/runtime.h>
#import "UIViewController+NavigationStyle.h"

#define BarTitleFont [UIFont systemFontOfSize:16]
#define BarItemFont [UIFont systemFontOfSize:14]
#define DefaultBarColor [UIColor whiteColor]
#define DefaultBarItemColor [UIColor blackColor]


@interface UIViewController()

@property (nonatomic, strong) UIColor *navColor;

@end

@implementation UIViewController (NavigationStyle)

#pragma mark - Method Swizzling
+ (void)load {

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        void (^__instanceMethod_swizzling)(Class, SEL, SEL) = ^(Class cls, SEL orgSEL, SEL swizzlingSEL){
            Method orgMethod = class_getInstanceMethod(cls, orgSEL);
            Method swizzlingMethod = class_getInstanceMethod(cls, swizzlingSEL);
            if (class_addMethod(cls, orgSEL, method_getImplementation(swizzlingMethod), method_getTypeEncoding(swizzlingMethod))) {

                class_replaceMethod(cls, orgSEL, method_getImplementation(swizzlingMethod), method_getTypeEncoding(swizzlingMethod));
            }
            else
            {
                method_exchangeImplementations(orgMethod, swizzlingMethod);
            }

        };

        {
            __instanceMethod_swizzling([self class], @selector(viewDidLoad), @selector(___viewDidLoad));
        }
        {
            __instanceMethod_swizzling([self class], @selector(viewWillAppear:), @selector(___viewWillAppear:));
        }
//        {
//            __instanceMethod_swizzling([self class], @selector(preferredStatusBarStyle), @selector(___preferredStatusBarStyle));
//        }
    });
}

- (void)___viewDidLoad{
    [self ___viewDidLoad];
    [self setNavigationWithColor:DefaultBarColor];
    if([UIDevice currentDevice].systemVersion.floatValue >= 7.0) {
        if (self.navigationController) {
            [self.navigationController.navigationBar setTranslucent:NO];
        }
    }
}

- (void)___viewWillAppear:(BOOL)animated {
    [self ___viewWillAppear:animated];
    [self setNavigationWithColor:self.navColor];
}

//-(UIStatusBarStyle)___preferredStatusBarStyle
//{
//    if(CGColorEqualToColor(self.navColor.CGColor, DefaultBarColor.CGColor))
//       return UIStatusBarStyleDefault;
//    else
//       return UIStatusBarStyleLightContent;
//}

- (void)setNavigationWithColorStr:(NSString *)colorStr {
    if (colorStr && colorStr.length == 7) {
        NSString *trueColor = [colorStr substringWithRange:NSMakeRange(1, colorStr.length - 1)];
        [self setNavigationWithColor:[UIViewController colorWithHexRGB:trueColor]];
    }
}

- (void)setNavigationWithColor:(UIColor *)color {
    if (color) {
        self.navColor = color;
        if(CGColorEqualToColor(color.CGColor, DefaultBarColor.CGColor))
        {
            //白色底色
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            [self.navigationController.navigationBar setBarTintColor:self.navColor];
            [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : DefaultBarItemColor, NSFontAttributeName : BarTitleFont}];
            [self.navigationItem.leftBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : DefaultBarItemColor, NSFontAttributeName : BarItemFont} forState:UIControlStateNormal];
            [self.navigationItem.leftBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : DefaultBarItemColor, NSFontAttributeName : BarItemFont} forState:UIControlStateHighlighted];
            [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : DefaultBarItemColor, NSFontAttributeName : BarItemFont} forState:UIControlStateNormal];
            [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : DefaultBarItemColor, NSFontAttributeName : BarItemFont} forState:UIControlStateHighlighted];
        }
        else
        {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            [self.navigationController.navigationBar setBarTintColor:self.navColor];
            [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : BarTitleFont}];
            [self.navigationItem.leftBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : BarItemFont} forState:UIControlStateNormal];
            [self.navigationItem.leftBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : BarItemFont} forState:UIControlStateHighlighted];
            [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : BarItemFont} forState:UIControlStateNormal];
            [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : BarItemFont} forState:UIControlStateHighlighted];
        }
    }
}


#pragma mark - setter && getter

- (UIColor *)navColor {
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setNavColor:(UIColor *)navColor{
        objc_setAssociatedObject(self, @selector(navColor), navColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


+ (UIColor *)colorWithHexRGB:(NSString *)inColorString
{
    UIColor *result = nil;
    unsigned int colorCode = 0;
    unsigned char redByte, greenByte, blueByte;
    
    if (nil != inColorString)
    {
        NSScanner *scanner = [NSScanner scannerWithString:inColorString];
        (void) [scanner scanHexInt:&colorCode]; // ignore error
    }
    
    redByte = (unsigned char) (colorCode >> 16);
    greenByte = (unsigned char) (colorCode >> 8);
    blueByte = (unsigned char) (colorCode); // masks off high bits
    result = [UIColor
              colorWithRed: (float)redByte / 0xff
              green: (float)greenByte/ 0xff
              blue: (float)blueByte / 0xff
              alpha:1.0];
    return result;
}

@end
