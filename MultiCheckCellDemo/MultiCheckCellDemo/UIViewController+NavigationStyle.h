//
//  UIViewController+NavigationStyle.h
//  kdweibo
//
//  Created by sevli on 16/9/8.
//  Copyright © 2016年 www.kingdee.com. All rights reserved.
//  Navigation 样式 --- NavigationStyle支持扩展

#import <UIKit/UIKit.h>



@interface UIViewController (NavigationStyle)


/**
 *  设置bar颜色
 *
 *  @param NSString
 */
- (void)setNavigationWithColorStr:(NSString *)colorStr;

/**
 *  设置bar颜色
 *
 *  @param UIColor
 */
- (void)setNavigationWithColor:(UIColor *)color;


@end
