//
//  ViewController.h
//  MultiCheckCellDemo
//
//  Created by fang.jiaxin on 16/12/16.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

