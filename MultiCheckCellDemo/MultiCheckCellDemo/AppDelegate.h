//
//  AppDelegate.h
//  MultiCheckCellDemo
//
//  Created by fang.jiaxin on 16/12/16.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

