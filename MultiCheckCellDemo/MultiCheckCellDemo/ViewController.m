//
//  ViewController.m
//  MultiCheckCellDemo
//
//  Created by fang.jiaxin on 16/12/16.
//

#import "ViewController.h"
#import "UITableView+Check.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"多选" style:UIBarButtonItemStylePlain target:self action:@selector(multiCheck:)];
}

-(void)multiCheck:(id)sender
{
    //设置这里（1），一共3处
    self.tableView.showCheck = !self.tableView.showCheck;
    self.tableView.checkBlock = ^(UITableViewCell *cell)
    {
        NSLog(@"%@ %@",cell.isCheck?@"check":@"unCheck",cell.cellTag);
    };
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 100;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        cell.textLabel.textColor = [UIColor blueColor];
        
        //设置这里（2），一共3处
        cell.parentTableView = tableView;
        cell.allowCheck = YES;
    }
    
    //设置这里（3），一共3处
    cell.cellTag = indexPath;
    
    cell.textLabel.text = [NSString stringWithFormat:@"%ld,%ld",indexPath.section,indexPath.row];
    
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //这里根据需要也可以设置
    if(self.tableView.showCheck)
    {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.isCheck = !cell.isCheck;
    }
}

@end
